# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171021140500) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "ar_internal_metadata", id: false, force: true do |t|
    t.string   "key",        limit: nil, null: false
    t.string   "value",      limit: nil
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "article_files", force: true do |t|
    t.integer  "article_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "article_files", force: true do |t|
    t.integer  "article_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "articles", force: true do |t|
    t.string   "title"
    t.integer  "author_id"
    t.string   "year"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "journal_id"
    t.string   "coauthors"
    t.string   "publication_data"
  end

  create_table "articles", force: true do |t|
    t.string   "title"
    t.integer  "author_id"
    t.string   "year"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "journal_id"
    t.string   "coauthors"
    t.string   "publication_data"
  end

  create_table "articles_chapters", force: true do |t|
    t.integer "article_id"
    t.integer "chapter_id"
  end

  create_table "articles_chapters", force: true do |t|
    t.integer "article_id"
    t.integer "chapter_id"
  end

  create_table "articles_reviews", force: true do |t|
    t.integer "article_id"
    t.integer "review_id"
  end

  create_table "articles_reviews", force: true do |t|
    t.integer "article_id"
    t.integer "review_id"
  end

  create_table "articles_sections", force: true do |t|
    t.integer "article_id"
    t.integer "section_id"
  end

  create_table "articles_sections", force: true do |t|
    t.integer "article_id"
    t.integer "section_id"
  end

  create_table "articles_subsections", force: true do |t|
    t.integer "article_id"
    t.integer "subsection_id"
  end

  create_table "articles_subsections", force: true do |t|
    t.integer "article_id"
    t.integer "subsection_id"
  end

  create_table "authors", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "patronymic"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authors", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "patronymic"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookmarks", force: true do |t|
    t.string   "title"
    t.text     "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "folder_id"
  end

  create_table "bookmarks", force: true do |t|
    t.string   "title"
    t.text     "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "folder_id"
  end

  create_table "chapters", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "chapters", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conference_healings", force: true do |t|
    t.integer  "conference_id",    limit: 8
    t.integer  "healing_month_id", limit: 8
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "conference_healings", ["conference_id"], name: "index_conference_healings_on_conference_id", using: :btree
  add_index "conference_healings", ["healing_month_id"], name: "index_conference_healings_on_healing_month_id", using: :btree

  create_table "conferences", force: true do |t|
    t.string   "title"
    t.text     "full_title"
    t.string   "place"
    t.integer  "year"
    t.integer  "month"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ball"
  end

  create_table "conferences", force: true do |t|
    t.string   "title"
    t.text     "full_title"
    t.string   "place"
    t.integer  "year"
    t.integer  "month"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ball"
  end

  create_table "conferences_users", id: false, force: true do |t|
    t.integer "conference_id"
    t.integer "user_id"
  end

  add_index "conferences_users", ["conference_id", "user_id"], name: "index_conferences_users_on_conference_id_and_user_id", unique: true, using: :btree
  add_index "conferences_users", ["conference_id", "user_id"], name: "index_conferences_users_on_conference_id_and_user_id", unique: true, using: :btree

  create_table "conferences_users", id: false, force: true do |t|
    t.integer "conference_id"
    t.integer "user_id"
  end

  add_index "conferences_users", ["conference_id", "user_id"], name: "index_conferences_users_on_conference_id_and_user_id", unique: true, using: :btree
  add_index "conferences_users", ["conference_id", "user_id"], name: "index_conferences_users_on_conference_id_and_user_id", unique: true, using: :btree

  create_table "faculties", force: true do |t|
    t.string   "name",       limit: nil
    t.text     "desc"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "folders", force: true do |t|
    t.string   "title"
    t.integer  "folder_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "folders", force: true do |t|
    t.string   "title"
    t.integer  "folder_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "folders_users", force: true do |t|
    t.integer "folder_id"
    t.integer "user_id"
  end

  create_table "folders_users", force: true do |t|
    t.integer "folder_id"
    t.integer "user_id"
  end

  create_table "healing_months", force: true do |t|
    t.integer  "year"
    t.integer  "month"
    t.integer  "kafedra_id",                 limit: 8
    t.integer  "user_id",                    limit: 8
    t.integer  "stachionar_kons_count"
    t.integer  "provireni_hv_count"
    t.integer  "obhodi_count"
    t.integer  "recenz_isthv"
    t.integer  "kilk_amb_consul"
    t.integer  "kilk_cons_pozam"
    t.integer  "mali_vtruch"
    t.integer  "operachiy"
    t.integer  "assist"
    t.integer  "conferences"
    t.integer  "dopovidey"
    t.integer  "sanaviachiya"
    t.integer  "analiz"
    t.integer  "clinic_conf_fate"
    t.integer  "clinic_conf_bump"
    t.integer  "pathologist_conf_fate"
    t.integer  "pathologist_conf_bump"
    t.integer  "uchast_atestat_conf"
    t.integer  "control_yakist_lik_diagn"
    t.integer  "uchast_expert_com_moz"
    t.integer  "robota_nad_clinic_protokol"
    t.integer  "plan_rab_dni"
    t.integer  "tabel_dni"
    t.decimal  "plan_rach_ball"
    t.decimal  "rach_tabel_ball"
    t.decimal  "vikonano_ball"
    t.decimal  "percent_ball"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "uchast_konsil"
    t.integer  "lizko_dni"
    t.text     "numb_his_prov_hv"
    t.text     "numb_his_kons_stach"
    t.text     "uchast_obh_data"
    t.text     "numb_amb_cart"
    t.string   "internet",                   limit: nil
    t.string   "vistup_in_zmi",              limit: nil
    t.string   "public_in_zmi",              limit: nil
    t.integer  "uch_rob_kolegii"
    t.integer  "work_count"
    t.integer  "guozddor"
    t.integer  "dopovidach_nauk_assosia"
    t.integer  "sluhach_nauk_assosia"
    t.integer  "klinik_nastanov"
    t.integer  "klinik_nakaziv"
    t.integer  "med_tech_doc"
    t.integer  "work_orgkomitet"
    t.integer  "dopovidach_nauk_conf"
    t.integer  "sluhach_nauk_conf"
    t.integer  "sertif_vitch"
    t.integer  "sertif_inozem"
    t.integer  "stazuv_nauk_conf"
    t.integer  "pidv_kvalik_in_ukraine"
    t.integer  "pidv_kvalik_out_ukraine"
    t.integer  "vistup_po_radio"
    t.integer  "vistup_po_tv"
    t.integer  "publik_gazeta"
    t.integer  "publik_nauk_popul_vidan"
    t.integer  "vidan_pam_broshurok"
    t.integer  "krugli_stolu"
    t.integer  "patent"
    t.integer  "inform_lust"
    t.integer  "method_rekom"
    t.integer  "novovved_moz"
    t.integer  "rozrob_na_kafedri"
    t.integer  "zapozicheni"
    t.integer  "org_method_robota"
    t.integer  "statyi_in_jornal"
    t.integer  "statyi_on_kordon"
    t.integer  "statyi_on_vitchizn"
    t.integer  "statyi_on_zbirniki"
    t.integer  "tezi_on_kordon"
    t.integer  "tezi_on_vitchizn"
    t.integer  "tezi_on_mono"
    t.integer  "tezi_on_pidruchniki"
    t.integer  "tezi_on_posibnik"
    t.integer  "tezi_on_zbirniki"
    t.integer  "ukrmedpat_rekom"
    t.integer  "ukrmedpat_informlist"
    t.integer  "ukrmedpat_patent"
    t.integer  "ukrmedpat_patent_vinahid"
    t.integer  "ukrmedpat_patent_model"
    t.integer  "ukrmedpat_avtor_pravo"
    t.integer  "zayavka_nauk_prachi"
    t.integer  "praktik_rozrob_on_kafedra"
    t.integer  "praktik_zapozicheno"
    t.text     "primitka"
  end

  add_index "healing_months", ["kafedra_id"], name: "index_healing_months_on_kafedra_id", using: :btree
  add_index "healing_months", ["user_id"], name: "index_healing_months_on_user_id", using: :btree

  create_table "journals", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "journals", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kafedras", force: true do |t|
    t.string   "name",       limit: nil
    t.text     "desc"
    t.integer  "faculty_id", limit: 8
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "kafedras", ["faculty_id"], name: "index_kafedras_on_faculty_id", using: :btree

  create_table "old_articles", force: true do |t|
    t.integer  "author_idi"
    t.integer  "article_idi"
    t.integer  "article_id_old"
    t.string   "coauthors"
    t.string   "journal"
    t.string   "nazvanie"
    t.string   "year"
    t.string   "shapka1"
    t.string   "shapka2"
    t.string   "month_tp"
    t.string   "que"
    t.boolean  "vpechati"
    t.string   "status"
    t.string   "type_art"
    t.string   "http_address"
    t.string   "person"
    t.string   "word_abs"
    t.string   "word_abstr"
    t.string   "pdf_abstr"
    t.string   "word"
    t.string   "word2"
    t.string   "pdf"
    t.string   "browser_file"
    t.string   "packet"
    t.string   "transl"
    t.date     "checked_up"
    t.text     "resume"
    t.boolean  "ref"
    t.boolean  "xerox"
    t.text     "mistake"
    t.string   "email"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_articles", force: true do |t|
    t.integer  "author_idi"
    t.integer  "article_idi"
    t.integer  "article_id_old"
    t.string   "coauthors"
    t.string   "journal"
    t.string   "nazvanie"
    t.string   "year"
    t.string   "shapka1"
    t.string   "shapka2"
    t.string   "month_tp"
    t.string   "que"
    t.boolean  "vpechati"
    t.string   "status"
    t.string   "type_art"
    t.string   "http_address"
    t.string   "person"
    t.string   "word_abs"
    t.string   "word_abstr"
    t.string   "pdf_abstr"
    t.string   "word"
    t.string   "word2"
    t.string   "pdf"
    t.string   "browser_file"
    t.string   "packet"
    t.string   "transl"
    t.date     "checked_up"
    t.text     "resume"
    t.boolean  "ref"
    t.boolean  "xerox"
    t.text     "mistake"
    t.string   "email"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_authors", force: true do |t|
    t.integer  "author_idi"
    t.integer  "old_author_idi"
    t.string   "author_name"
    t.string   "short_i"
    t.string   "short_o"
    t.string   "imya"
    t.string   "otch"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_authors", force: true do |t|
    t.integer  "author_idi"
    t.integer  "old_author_idi"
    t.string   "author_name"
    t.string   "short_i"
    t.string   "short_o"
    t.string   "imya"
    t.string   "otch"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_level4s", force: true do |t|
    t.integer  "punkt_idi"
    t.integer  "level_4_idi"
    t.string   "level_4"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_level4s", force: true do |t|
    t.integer  "punkt_idi"
    t.integer  "level_4_idi"
    t.string   "level_4"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_obzors", force: true do |t|
    t.integer  "n_from_razdel"
    t.integer  "n_obzor"
    t.string   "obzor_podst"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_obzors", force: true do |t|
    t.integer  "n_from_razdel"
    t.integer  "n_obzor"
    t.string   "obzor_podst"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position1s", force: true do |t|
    t.integer  "article_idi"
    t.integer  "position1_idi"
    t.string   "position1"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position1s", force: true do |t|
    t.integer  "article_idi"
    t.integer  "position1_idi"
    t.string   "position1"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position2s", force: true do |t|
    t.integer  "position1_idi"
    t.integer  "position2_idi"
    t.string   "position2"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position2s", force: true do |t|
    t.integer  "position1_idi"
    t.integer  "position2_idi"
    t.string   "position2"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position3s", force: true do |t|
    t.integer  "position2_idi"
    t.integer  "position3_idi"
    t.string   "position3"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position3s", force: true do |t|
    t.integer  "position2_idi"
    t.integer  "position3_idi"
    t.string   "position3"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position4s", force: true do |t|
    t.integer  "position3_idi"
    t.integer  "position4_idi"
    t.string   "position4"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position4s", force: true do |t|
    t.integer  "position3_idi"
    t.integer  "position4_idi"
    t.string   "position4"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_punkts", force: true do |t|
    t.integer  "n_from_obzor"
    t.integer  "n_punct"
    t.string   "punct_podst"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_punkts", force: true do |t|
    t.integer  "n_from_obzor"
    t.integer  "n_punct"
    t.string   "punct_podst"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_razdels", force: true do |t|
    t.integer  "n_razdel"
    t.string   "razdel_podst"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_razdels", force: true do |t|
    t.integer  "n_razdel"
    t.string   "razdel_podst"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permission_roles", force: true do |t|
    t.integer  "role_id",       limit: 8
    t.integer  "permission_id", limit: 8
    t.boolean  "active"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "permission_roles", ["permission_id"], name: "index_permission_roles_on_permission_id", using: :btree
  add_index "permission_roles", ["role_id"], name: "index_permission_roles_on_role_id", using: :btree

  create_table "permissions", force: true do |t|
    t.string   "name",          limit: nil
    t.text     "desc"
    t.string   "action",        limit: nil
    t.string   "subject_class", limit: nil
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "personals", force: true do |t|
    t.string   "type_pers",     limit: nil
    t.string   "first_name",    limit: nil
    t.string   "last_name",     limit: nil
    t.string   "patronymic",    limit: nil
    t.string   "position",      limit: nil
    t.string   "academ_degree", limit: nil
    t.string   "category",      limit: nil
    t.decimal  "stavka_assist",             default: 0.0
    t.decimal  "stavka_docent",             default: 0.0
    t.decimal  "stavka_prof",               default: 0.0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "user_id",       limit: 8
    t.integer  "kafedra_id",    limit: 8
  end

  add_index "personals", ["kafedra_id"], name: "index_personals_on_kafedra_id", using: :btree
  add_index "personals", ["user_id"], name: "index_personals_on_user_id", using: :btree

  create_table "reports", force: true do |t|
    t.integer  "user_id"
    t.integer  "year"
    t.integer  "month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.integer  "user_id"
    t.integer  "year"
    t.integer  "month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reviews", force: true do |t|
    t.string   "title"
    t.integer  "chapter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reviews", force: true do |t|
    t.string   "title"
    t.integer  "chapter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name",       limit: nil
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "sections", force: true do |t|
    t.string   "title",      default: "title is missing"
    t.integer  "review_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sections", force: true do |t|
    t.string   "title",      default: "title is missing"
    t.integer  "review_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subsections", force: true do |t|
    t.string   "title",      default: "title is missing"
    t.integer  "section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subsections", force: true do |t|
    t.string   "title",      default: "title is missing"
    t.integer  "section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "login",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "patronymic"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree
  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "login",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "patronymic"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree
  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", force: true do |t|
    t.integer  "user_id",    limit: 8
    t.integer  "role_id",    limit: 8
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "users_roles", ["role_id"], name: "index_users_roles_on_role_id", using: :btree
  add_index "users_roles", ["user_id"], name: "index_users_roles_on_user_id", using: :btree

end
